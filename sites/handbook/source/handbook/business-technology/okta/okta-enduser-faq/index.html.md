---
layout: handbook-page-toc
title: "Okta FAQs"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Below are frequently asked questions about Okta and GitLab's implementation of it. If you have an additional question that is not listed here, please ask in #it_help in Slack.

## How do I use Okta to log into an application?

1. Visit [gitlab.okta.com](https://gitlab.okta.com) and you'll see a list of applications available to you.
2. Search for the application you want to log into and then click on it.
3. This will open a new tab and should automatically log you in.

_If you are having issues logging into an application with Okta SSO, please post in the `#it_help` channel in Slack. Please include a screenshot of the error that you're seeing._

## How will Okta support GitLab's account creation?

The activation workflow is triggered by the creation of account within Workday.

## Which browsers does Okta work on?

Okta supports the following Web Browsers: Chrome, Firefox, and Safari. 

## What is a plugin?

Plugins are applications that can easily be installed and are used as a part of a web browser.

## Do I have to use the Okta plugin?

No, but it is recommended to improve the Okta user experience.
## Is it safe to install the Okta plugin?

Yes, the Okta plugin is very safe to install.

## How do I request an app to be added or updated?

Application Requests are managed via a [Change Management Issue](https://gitlab.com/gitlab-com/business-technology/change-management/-/issues/new?issue%5Bmilestone_id%5D=#) within the Business Technology team. Please use the `okta_new_app_request` template to ensure all required information fields are inputted. 

[All new software must go through Procurement](https://about.gitlab.com/handbook/finance/procurement/new-software/) before being added to Okta.

**Things to consider before opening a request:**
   - Has the Vendor Representative (AE, CSM) confirmed that SSO, SAML or SCIM is supported?
      - Please set up a call with a technical contact for the vendor and start an email thread to confirm available features for SSO and automated user management.
   - How should user creation, updates, and deactivation be processed?
   - Which groups of users need access to this? An exact list of existing Google groups or a list of users will be needed.
   - **Timeline:** Please allow up to _4 weeks_ for an application integration to be completed. If the request is urgent, please tag your manager and explain the urgency and requested date of completion.

## How can I change the order in which my apps appear?

To change the order of your apps, click and hold on an app icon, then drag and drop the app to the location you would like it to be displayed.


## Does Okta store our information entered into apps like Workday, Greenhouse etc?

No, Okta only acts as an integrator to the various apps. The information that the user stores inside of the app, is not accessible by Okta. The usernames and passwords (user credentials) are encrypted using both an industry-­standard encrypted AES and a randomly generated symmetric key.

## What’s the difference between SWA and SAML?

**SWA** - A SWA integration provides single sign-on for apps that don't support proprietary federated sign-on methods; it works with any web-based app.  If your SWA app integration is successful, the following happens when end-users click the app chiclet:

- The username and password fields are populated.
- Users are signed in to the app automatically.

**SAML** - A SAML (Security Assertion Markup Language) integration provides Federated Authentication standards that allow end users one-click access to the app.
SAML is an XML-based standard for exchanging authentication and authorization data between an identity provider (IdP) such as Okta, and a service provider (SP).
If your Application supports SAML, then this is a better Authentication Protocol over SWA. When SAML is enabled Okta will be the Authentication provider for the application, also, this Authentication Protocol will give you more control over your users Authentication behavior.
Okta can authenticate to Cloud Applications using SAML Assertions, a “passwordless” assertion method that is authorised based on a trust decision from Okta based on its authentication policy. This reduces the exposure for credential leakage and eases auditability.

## How do self-service applications work with Okta?

There is an example of this in the Application configuration Video linked on the Okta page. In short, click on the Add Apps button, search for the App you want to request, and click the request button. As a best practice, link in the Access Request issue created so that the Application owner understands the requirements of the request!


## What is my username and password for GitLab Okta?

At GitLab, your GitLab email address is your username. If you've forgotten your password, use the 'Forgot password' link at the bottom of the sign‐in page to generate a new one.

## How do I make a safe password?

Okta supports strong passwords through the use of password complexity rules and length, consistent with our password policy. Please follow [GitLab’s Password Policy guidelines](/handbook/security/#GitLab-password-policy-guidelines) to determine the rules.


## Why do I have to input my password for some apps and not others?

With Okta you can access your applications through a single, unified dashboard. Access to these applications is delivered through single sign-­on (SSO) technology via either Security Assertion Markup Language (SAML) or Okta’s Secure Web Authentication (SWA) technology.

With SWA, you may need to enter your username and password upon the first use of that application. If an app requires you to make a password change, you should do so within the Okta dashboard. With SAML, Okta automatically passes on access via a token, so you don’t need to manually make a change when the app requires an update.

## How do I change my password for an app?

To change your password for a specific application, click the three dots in the upper right corner, which should open a menu that gives you the option to change your password for that specific application. 

## Can I be confident my password is safe?

Yes, nobody (including GitLab administrators or IT support) have access or visibility into your password data.

## Where & how is my username and password stored?

Okta uses strong encryption to secure data and uses strong (256-­bit AES) encryption for username and password credentials. This information is stored and maintained by Okta.

## How long does my login last until I need to re-authenticate?

Okta has three controlled re-authentication parameters -- Factor Lifetime, Maximum Session Lifetime, and Session Lifetime.

Factor Lifetime: Setting a factor lifetime is a way for end users to sign out for the amount of time noted in the Factor Lifetime and not have to authenticate again with MFA at the next sign in. End users must check a box to confirm that the setting should be applied. We have configured this to be 15 minutes. If you are logging on with a machine that isn't your usual device, make sure you don't check this button!

Maximum Session Lifetime: The maximum session time before an authentication prompt is triggered. We have configured this to be 16 hours.

Session Lifetime: The maximum idle time before an authentication prompt is triggered. This is configured to be 4 hours. Idle time is defined as clicking something within the Okta dashboard (to launch a new app, for example).

If you are having issues with excessive authentication requests, please reach out in #it_help.

## Can my GitLab Okta admin see my login information?

The GitLab administrator can see your username, but he or she does not have access to your password or any other credentials or devices you use to authenticate.

## Will Okta support our password policy?

Yes, Okta has been configured such that - it adheres to the GitLab password policy and restrictions.

## Will users only use 1Password to store Okta credentials? As part of getting set up in Okta, will we move all of our stored creds from 1Password into Okta (and then delete from 1Password)?

For many applications, that’s correct. The goal will be to focus on Logins in 1Password and migrate as many of them (particularly shared passwords!) to Okta. There are other things like Secure Notes that don’t translate, so we won’t be completely getting rid of 1Password.

## Any app that is already integrated and existing in Okta can be deleted from 1Password?

Yes, once you’ve put the credentials in Okta, you may delete it from 1Password.

## Is it acceptable to continue using Google Oauth login for some of the apps (with its own 2FA)?

Yes, this is fine. Our end state will be to use Okta for most/all applications, but in the meantime existing authentication methods are fine. Once we have broader user adoption of Okta, we’ll be taking on migration of some of these apps to use Okta instead of Google OAuth, where this makes the most sense.

## Will Okta only be used for work-related credentials?  If so, how will it be enforced?

At this time there is nothing stopping someone from using personal credentials. See the video created about configuring Apps for more details.

## Does Okta have any visibility into MFA?

MFA is strictly enforced on Okta. This acts as a gateway to the services it manages.  So those services, in effect require MFA via Okta login.

## Will Okta be used to prevent logins from particular devices or conditions?

Okta has capacity to do that and we have configured [Device Trust](https://about.gitlab.com/handbook/business-technology/okta/#device-trust).

## Why does my Okta session expire but some of the apps are still open?

Okta does not log you out of your applications even though you might be logged out of your Okta session.

## What happens if Okta goes down?

Okta is built on an “Always On” architecture. However, if their services were to go down, you would not be able to log in to your GitLab Okta platform and access your applications via Single Sign‐On. However, some applications might still be accessible through a direct link.
When Okta is down, basically GitLab will be down. Any mitigation strategy would enlarge our security surface area.

## Even though it’s rare or unlikely, what is our policy with regard to what employees should do in the event Okta is down? Just wait for it to come back, or is there some backup plan?

When Okta is down we are down. Any mitigation strategy would enlarge our security surface area. Okta is built on an “Always On” architecture. However, if their services were to go down, you would not be able to log in to your GitLab Okta platform and access your applications via Single Sign‐On. However, some applications might still be accessible through a direct link.

## Who do I contact in case of Okta emergencies?

Please reach out in the #it_help channel on Slack.

## Is there any automated reporting? (e.g. Can we access logs emailed to the team and use that to kick off an access review?)

No, this does not exist within Okta.  But this should be something security automation team can help with. There are some automated security notifications already enabled for things like MFA or password changes, as well as connections from new and unrecognised devices. GitLab is able to perform reporting on these based on built-in reporting.

## What are some things Okta won’t be able to do yet?

Automating access requests is one such thing. This is not going to be magically solved. Manager approval is not possible with Okta as yet. We are working towards other ways and means to achieve this.

## Does Okta integrate with a VPN provider that will allow everyone at GitLab (team members) to use a company-wide VPN?

There is capacity to do this, but further research is needed to understand the application use case for this. At this stage, requirement for VPN is not enabled.

## For those of us who used our GitLab personal account when we were onboarded, what happens to my GitLab account when I am off-boarded? Does Okta change anything about this, or any policy changes around accounts that might make it important for me to use a company account name?

We do not remove GitLab accounts currently for off-boarding, currently we remove users from GitLab groups. When offboarding happens, your Okta account and related accounts are Deactivated or Suspended.

